package com.example.kety.model

interface CategoriesApi {
    suspend fun getCategories(): List<String>
}