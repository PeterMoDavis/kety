package com.example.kety.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object CategoriesRepo {
    private val categories = listOf<String>("Rank", "Hot", "Love", "Secrets", "Friends", "Placebos", "Candy-Cane", "Gary", "Something", "Yes")
    private val categoriesApi = object : CategoriesApi{
        override suspend fun getCategories(): List<String> {
                return categories
        }
    }
    suspend fun getCategories():List<String> = withContext(Dispatchers.IO) {
        delay(500)
        categoriesApi.getCategories()
    }
}