package com.example.kety.model

interface SkinTypeApi {
    suspend fun getSkinTypes(): List<String>
}