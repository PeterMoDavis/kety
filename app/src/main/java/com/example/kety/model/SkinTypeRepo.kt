package com.example.kety.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object SkinTypeRepo {
    private val skinTypes = listOf<String>(
        "Oily",
        "Flaky",
        "Dry",
        "Supple",
        "Refreshed",
        "Moisturized",
        "Refreshed",
        "Itchy",
        "Scaly",
        "Glowing"
    )
    private val skinTypeApi = object : SkinTypeApi {
        override suspend fun getSkinTypes(): List<String> {
            return skinTypes
        }
    }

    suspend fun getSkinTypes(): List<String> = withContext(Dispatchers.IO){
        delay(500)
        skinTypeApi.getSkinTypes()
    }
}