package com.example.kety.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kety.model.CategoriesRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CategoriesViewModel : ViewModel() {
    private val repo = CategoriesRepo
    private val _categories = MutableLiveData<List<String>>()
    val categories: LiveData<List<String>> get() = _categories

    fun getCategories() = viewModelScope.launch(Dispatchers.Main) {
        val categories = repo.getCategories()
        _categories.value = categories
    }
}