package com.example.kety.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kety.model.SkinTypeRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SkinTypeViewModel : ViewModel() {
    private val repo = SkinTypeRepo
    private val _skinTypes = MutableLiveData<List<String>>()
    val skinTypes: LiveData<List<String>> get() = _skinTypes

    fun getSkinTypes() = viewModelScope.launch(Dispatchers.Main) {
        val skinTypes = repo.getSkinTypes()
        _skinTypes.value = skinTypes
    }
}