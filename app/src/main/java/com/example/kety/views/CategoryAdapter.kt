package com.example.kety.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kety.databinding.ItemCategoryBinding
import com.example.kety.model.CategoriesRepo.getCategories
import com.example.kety.viewmodel.CategoriesViewModel

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var categories = mutableListOf<String>()
    private var adapterListener: ((String)-> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CategoryViewHolder {
        val binding = ItemCategoryBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(categoryViewHolder: CategoryViewHolder, position: Int) {
        val category = categories[position]
        categoryViewHolder.loadCategory(category)
        categoryViewHolder.setClickListener {
            adapterListener?.invoke(category)
        }
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    fun addCategory(category: List<String>){
        this.categories = category.toMutableList()
        notifyDataSetChanged()
    }

    fun addItemClickListener(function: (String) -> Unit){
        adapterListener = function
    }

    class CategoryViewHolder(
        private val binding: ItemCategoryBinding
    ) : RecyclerView.ViewHolder(binding.root){
        fun loadCategory(category: String){
            binding.itemCategoryText.text = category
        }
        fun setClickListener(clickListener: View.OnClickListener?){
            binding.itemCategory.setOnClickListener(clickListener)
        }
    }
}