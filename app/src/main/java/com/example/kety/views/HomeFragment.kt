package com.example.kety.views

import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import com.example.kety.databinding.FragmentHomeBinding
import com.example.kety.viewmodel.CategoriesViewModel
import com.example.kety.viewmodel.SkinTypeViewModel

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val categoriesViewModel by viewModels<CategoriesViewModel>()
    private val skinTypeViewModel by viewModels<SkinTypeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoriesViewModel.getCategories()
        skinTypeViewModel.getSkinTypes()

        categoriesViewModel.categories.observe(viewLifecycleOwner) { category ->
            binding.categoriesLinear.apply {
                adapter = CategoryAdapter().apply {
                    addCategory(category)
                    addItemClickListener { category: String ->
                        findNavController().navigate(
                            HomeFragmentDirections.actionHomeFragmentToDetailFragment(
                                category
                            )
                        )
                    }
                }
            }
        }
        skinTypeViewModel.skinTypes.observe(viewLifecycleOwner) { skinTypes ->
            binding.skinTypeLinear.apply {
                adapter = SkinTypeAdapter().apply {
                    addSkinTypes(skinTypes)
                    addItemClickListener { skin: String ->
                        findNavController().navigate(
                            HomeFragmentDirections.actionHomeFragmentToDetailFragment(
                                skin
                            )
                        )
                    }
                }
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}