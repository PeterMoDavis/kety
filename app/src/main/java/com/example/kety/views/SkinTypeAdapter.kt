package com.example.kety.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import androidx.recyclerview.widget.RecyclerView
import com.example.kety.databinding.ItemSkinTypeBinding

class SkinTypeAdapter : RecyclerView.Adapter<SkinTypeAdapter.SkinTypeViewHolder>() {

    private  var skinTypes = mutableListOf<String>()
    private var adapterListener: ((String)-> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SkinTypeViewHolder {
        val binding = ItemSkinTypeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return SkinTypeViewHolder(binding)
    }

    override fun onBindViewHolder(skinTypeViewHolder: SkinTypeViewHolder, position: Int) {
        val skinType = skinTypes[position]
        skinTypeViewHolder.loadSkinType(skinType)
        skinTypeViewHolder.setClickListener {
            adapterListener?.invoke(skinType)
        }
    }

    override fun getItemCount(): Int {
        return skinTypes.size
    }

    fun addSkinTypes(skinType: List<String>){
        this.skinTypes = skinType.toMutableList()
        notifyDataSetChanged()
    }

    fun addItemClickListener(function: (String) -> Unit){
        adapterListener = function
    }

    class SkinTypeViewHolder(
        private val binding: ItemSkinTypeBinding
    ) : RecyclerView.ViewHolder(binding.root){

        fun loadSkinType(skinType: String){
            binding.itemSkinTypeText.text = skinType
        }
        fun setClickListener(clickListener: View.OnClickListener?){
            binding.itemSkinType.setOnClickListener(clickListener)
        }
    }
}